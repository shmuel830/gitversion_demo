
import os
import random



def generate_password(length):
    characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()"
    password = ''.join(random.choice(characters) for i in range(length))
    return password

def is_prime(num):
    if num <= 1:
        return False
    for i in range(2, int(num ** 0.5) + 1):
        if num % i == 0:
            return False
    return True


print("Welcome to the Python app and GitVersion...")

length = int(input("Enter the length of the password you want to generate number: "))
password = generate_password(length)
print("Your generated password is:", password)

number = int(input("Enter a number to check if it's prime: "))
if is_prime(number):

    print(number, "is a prime number!!")

else:
    print(number, "is not a prime number!!")

print("Current working directory...:", os.getcwd())

# Test #1
# Test #2
# Test #3
# Test #4
# Test From dev
# Test From Dev #2
# Test from dev #3
