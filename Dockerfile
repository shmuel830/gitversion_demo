# Use the official Python image from the Docker Hub
FROM python:3.8-slim

# Copy the current directory contents into the container at /app
COPY . /app

# Set the working directory to /app
WORKDIR /app

# Run app.py when the container launches
CMD ["python", "app.py"]

